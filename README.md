# Hello, I'm Adrien 👋

I'm a passionate Fullstack Developer, constantly learning and adapting to new technologies to create high-quality web applications. My focus is to create user-friendly and scalable web solutions that deliver a seamless experience.

## My Skills

### Programming Languages

![JavaScript](https://img.shields.io/badge/-JavaScript-black?style=flat-square&logo=javascript)
![Python](https://img.shields.io/badge/Python-3776AB?logo=python&logoColor=white)

### Frontend Development

![React](https://img.shields.io/badge/-React-black?style=flat-square&logo=react)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=ffffff)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3)

### Backend Development

![Node.js](https://img.shields.io/badge/-Node.js-black?style=flat-square&logo=Node.js)
![Express.js](https://img.shields.io/badge/-Express.js-yellow?style=flat-square&logo=express)

### Databases

![MongoDB](https://img.shields.io/badge/-MongoDB-green?style=flat-square&logo=mongodb)
![MySQL](https://img.shields.io/badge/-MySQL-black?style=flat-square&logo=mysql)
![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-blue?style=flat-square&logo=postgresql)

### Other Skills
- Git/GitHub
- Agile Methodologies

## Why Work With Me?

I believe that a great developer not only has excellent technical skills, but also excellent communication and interpersonal skills. As a senior developer, I have honed my ability to effectively communicate with clients and team members, listen to their needs and concerns, and provide tailored solutions to meet their requirements. 

In addition, I am constantly learning and staying up-to-date with the latest technologies and industry trends. I have a strong focus on writing clean, modular, and maintainable code that is easy to read and understand.

## Contact Me

If you're interested in working with me or have any questions, please feel free to reach out to me via email at [frjr17@criptext.com](mailto:frjr17@criptext.com).
